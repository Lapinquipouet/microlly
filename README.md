MICROLLY - SEBASTIEN COCHET ET CORENTIN BOONAERT
-> Ce projet est un projet de micro-blog, qui permet à un utilisateur de s'inscrire sur le site, de pouvoir se connecter, se déconnecter et voir des articles. 
-> Ce projet intervient dans le cadre du cours de M SAMY DELAHAYE, professeur à l'IUT d'Amiens en "Web Dynamique". 
-> Les fichiers de ce projet sont en langage Python. 

Les packages utilisés pour ce projet de micro-blog sont :
	- Flask
	- Flask-Wtf
	- Peewee
	- Wtf-Peewee
	- Wtforms
	- Flask-Login

Les fonctionnalités implantées pour ce projet sont : 
	- Page d'Accueil
	- Système d'inscription des utilisateurs
	- Système de connexion des utilisateurs
	- Table SQL : informations des utilisateurs et publication
	- Onglet permettant d'aller voir les publications
	- Utilisation d'un BootsTrap 
	- Système de "logout" avec une route vers /logout dans le app.py

En vous souhaitant une bonne navigation sur notre site,
Cordialement,


Corentin et Sébastien, fondateurs de Microlly
