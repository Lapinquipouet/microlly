from wtfpeewee.orm import model_form
from models import User
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField


class LoginForm(FlaskForm):
    username = StringField()
    password = PasswordField()


RegisterUserForm = model_form(User)