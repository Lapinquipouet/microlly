import datetime
from peewee import *
from flask_login import UserMixin


database = SqliteDatabase('DATABASE')

class BaseModel(Model):
    class Meta:
        database = database


class User(UserMixin, BaseModel):
    nom_utilisateur = CharField(unique=True)
    prenom = CharField()
    nom = CharField()
    date_anniversaire = DateField()
    mot_de_passe = CharField()
    email = CharField()

class Publication(BaseModel):
    titre_publication = CharField()
    corps_publication = CharField()
    creation_publication = DateField()
    modification_publication = DateField()

def create_tables():
    with database:
        database.create_tables([User, Publication, ])

def drop_tables():
    with database:
        database.drop_tables([User, Publication, ])        
