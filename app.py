from flask import Flask, render_template, flash, redirect, url_for, request
import click
from peewee import *
from models import create_tables, drop_tables, User
from forms import RegisterUserForm, LoginForm
from flask_login import LoginManager, login_user, login_required, logout_user

login_manager = LoginManager()

app = Flask(__name__)
login_manager.init_app(app)
login_manager.login_view = 'connexion'

@login_manager.user_loader
def load_user(user_id):
    return User.get(id=user_id)

app.secret_key = 'hello'

@app.cli.command()
def initdb():
    create_tables()
    click.echo('Database created')

@app.cli.command()
def dropdb():
    drop_tables()
    click.echo('Database dropped')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/blog')
def blog():
    return render_template('blog.html')

@app.route('/connexion', methods=['GET', 'POST'])
def connexion(name=None):
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_or_none(
            User.nom_utilisateur == form.username.data,
            User.mot_de_passe == form.password.data
        )
        print(user)
        print(user.nom_utilisateur)
        # Login and validate the user.
        # user should be an instance of your `User` class
        if user:
            login_user(user)
            flash('Vous êtes connectés !')
            return redirect(url_for('compte'))
        else:
            flash("Invalid credential")
    return render_template('connexion.html', form=form)


@app.route('/inscription', methods=['GET', 'POST', ]) 
def inscription(name=None):
    if request.method == 'POST':
        form = RegisterUserForm(request.form)
        if form.validate():
            user = User()
            form.populate_obj(user)
            user.save()
            flash('User created')
            return redirect(url_for('connexion'))
    else:
        form = RegisterUserForm()
    return render_template('inscription.html', form=form)

@app.route('/compte')
@login_required
def compte():
    return render_template('compte.html')


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

#ROUTE MEME QUE INSCRIPTION -> RETOURNER COOKIE / TOKEN / URL VERS CONNEXION / ENTREE TOKEN / GENERATION D'UN CARACTÈRE /
#TOKEN AVEC DATE VALIDITÉ AVEC UN IF 